using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using srs.fuzz.Configuration;
using srs.fuzz.Internal;
using srs.fuzz.utilities;

namespace srs.fuzz.tests
{
    public class ApiPlaygroundTestHost : hosts.nunit.NUnitHost<ApiPlayground> { }

    internal class AggregatedMeasurement
    {
        public TimeSpan Total { get; private set; }
        public uint Iterations { get; private set; }
        public TimeSpan Average => Total / Iterations;
        public TimeSpan Max { get; private set; }
        public TimeSpan Min { get; private set; } = TimeSpan.MaxValue;
        public void Add(TimeSpan span)
        {
            Total += span;
            Iterations++;
            if (Min > span) Min = span;
            if (span > Max) Max = span;
        }
    }

    public class ApiPlayground : TestRunner
    {
        private readonly AggregatedMeasurement measurement = new AggregatedMeasurement();
        private readonly string filename = $"test run {DateTime.Now.Ticks}.txt";
        protected override TestConfig Configure()
        {
            var builder = ConfigBuilder.For(() => new ClassUnderTest(), Recycle.PerCase); // also PerSuite & PerRun

            var suite = builder.CreateSuite("independent");
            var method = suite.RegisterMethod(nameof(ClassUnderTest.Add));
            method.ConfigureParameter("l").ConsumingRandom(-100, 100, 100);
            method.ConfigureParameter(1).ConsumingSequential(-100, 0);
            method.Expect(args => args.At<short>(0) + args.At<short>(1));

            method = suite.RegisterMethod(nameof(ClassUnderTest.ThrowOnOdd));
            method.ConfigureParameter("x").ConsumingExhaustive();
            method
                .Throws<AssertionException>(args => args.At<ushort>(0) % 2 == 1)
                .Expect(args => args[0], args => true);

            suite = builder.CreateSuite("chain").With(() => new ClassUnderTest(), Recycle.PerCase);
            method = suite.RegisterMethod("CreateEnvelopeForTest");
            method.ConfigureParameter(0).ConsumingSequential(1, 50);
            method.ConfigureParameter(1).ConsumingCustom(new object[] { "simple", "not simple" });
            method.ConfigureParameter(2).ConsumingCustom(RandomStringProvider.Alpha(3, 5, 50));
            method.ConfigureParameter(3).ConsumingCustom(RandomStringProvider.Alphanumeric(3, 6, 50));

            // Process emulation - creates chain
            suite
                .RegisterMethod("IsProcessed")
                .Expect(args => args.At<Dictionary<string, string>>(0)["id"].Contains("1"))
                .ConfigureParameter(0)
                .ConsumingOutput("CreateEnvelopeForTest");
            suite
                .RegisterMethod("IsSimple")
                .DependsOn("IsProcessed", false)
                .Expect(args => args.At<Dictionary<string, string>>(0)["a"] == "simple")
                .ConfigureParameter(0)
                .ConsumingOutput("CreateEnvelopeForTest");
            suite
                .RegisterMethod("ProcessSimple")
                .DependsOn("IsSimple", true)
                .Expect(args => args.At<Dictionary<string, string>>(0)["b"] == args.At<Dictionary<string, string>>(0)["c"])
                .ConfigureParameter(0)
                .ConsumingOutput("CreateEnvelopeForTest");
            suite
                .RegisterMethod("ProcessSpecial")
                .DependsOn("IsSimple", false)
                .Expect(args => args.At<Dictionary<string, string>>(0)["b"] != args.At<Dictionary<string, string>>(0)["c"])
                .ConfigureParameter(0)
                .ConsumingOutput("CreateEnvelopeForTest");

            return builder.CreateConfig();
        }

        public override object ExecuteCase(Method method, object target, object[] args)
        {
            // enhancing basic executor - add timeout for case
            try
            {
                return Task.Run(
                        () => base.ExecuteCase(method, target, args),
                        new CancellationTokenSource(TimeSpan.FromSeconds(1)).Token)
                        .Result;
            }
            catch (TaskCanceledException)
            {
                return null;
            }
            catch (AggregateException e) 
                when (e.InnerException is TaskCanceledException || e.InnerExceptions.OfType<TaskCanceledException>().Any())
            {
                return null;
            }
        }
    }
}