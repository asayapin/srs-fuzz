using System.Collections.Generic;
using NUnit.Framework;

namespace srs.fuzz.tests
{
    public class ClassUnderTest
    {
        public int Add(short l, short r) => (l + r);
        public ushort ThrowOnOdd(ushort x) => x % 2 == 0 ? x : throw new AssertionException("ha-ha");

        public Dictionary<string, string> CreateEnvelopeForTest(int id, string a, string b, string c) => new Dictionary<string, string>{
            ["a"] = a, ["b"] = b, ["c"] = c, ["id"] = id.ToString()
        };

        public bool Process(Dictionary<string, string> envelope)
        {
            if (IsProcessed(envelope)) return false;
            return IsSimple(envelope) ? ProcessSimple(envelope) : ProcessSpecial(envelope);
        }

        public bool IsProcessed(Dictionary<string, string> envelope) => envelope["id"].Contains("1");
        public bool IsSimple(Dictionary<string, string> envelope) => envelope["a"] == "simple";
        public bool ProcessSimple(Dictionary<string, string> envelope) => envelope["b"] == envelope["c"];
        public bool ProcessSpecial(Dictionary<string, string> envelope) => envelope["b"] != envelope["c"];
    }
}