# YAFF - yet another fuzzing framework

**Objective:** create a framework for simple configurable class fuzzing testing

**Points:**
- target is to create a framework for test running - not benchmarking, not anything else. just create test suites and run them over some domain
- Nunit may not be preferred framework, thus everything should be split:
  - yaff.core - configuration, domain building, dependencies establishment etc
  - yaff.nunit - Nunit bindings
  - yaff.mstest - MSTest bindings
  - yaff.xunit - xUnit bindings
  - etc
- exhaustive testing is possible only on domains with less ~2^30 items, thus default strategy is random domain (though exhaustive is present)
- allow to configure custom domain
- allow to configure dependencies and chains
  - some methods may take same input as other methods
  - some methods may take other methods' output as input
  - some methods may need to be run only if other methods' outputs satisfy some constraints
- allow to mix testing strategies for different parameters of one method
- allow to decorate runner method with some logic (make Run virtual)
- to allow chains, results must be cached - but once chain is ran, cache can be dropped
  - thus, dependencies must be tracked


`ClassUnderTest` example

```
public class ClassUnderTest{
    public short Add(short l, short r) => (short) (l + r);
    public uint ThrowOnOdd(uint x) => x % 2 == 0 ? x : throw new AssertionException("ha-ha");

    public Dictionary<string, string> CreateEnvelopeForTest(int id, string a, string b, string c) => new Dictionary<string, string>{
        ["a"] = a, ["b"] = b, ["c"] = c, ["id"] = id.ToString()
    };

    public bool Process(Dictionary<string, string> envelope)
    {
        if (IsProcessed(envelope)) return false;
        return IsSimple(envelope) ? ProcessSimple(envelope) : ProcessSpecial(envelope);
    }

    public bool IsProcessed(Dictionary<string, string> envelope) => envelope["id"].Contains("1");
    public bool IsSimple(Dictionary<string, string> envelope) => envelope["a"] == "simple";
    public bool ProcessSimple(Dictionary<string, string> envelope) => envelope["b"] == envelope["c"];
    public bool ProcessSpecial(Dictionary<string, string> envelope) => envelope["b"] != envelope["c"];
}
```

`Test` example:

```
public class Test : TestRunner {
    public TestConfig Configure(){
        ConfigBuilder builder = ConfigBuilder.For<ClassUnderTest>(factory: () => new ClassUnderTest(), recycle: Recycle.PerCase); // also PerSuite & PerRun

        TestSuite suite = builder.CreateSuite(name: "independent");
        MethodConfigBuilder method = suite.RegisterMethod(name: nameof(ClassUnderTest.Add));
        ParameterConfigBuidler parameter = method.ConfigureParameter(name: "l").Consumig(Domain.Random, from: -100, to: 100);
        method.ConfigureParameter(index: 1).Consuming(Domain.Sequential, from: -200, to: 0);
        method.Expect(what: args => args.At<short>(0) + args.At<short>(1));

        method = suite.RegisterMethod(nameof(ClassUnderTest.ThrowOnOdd));
        method.ConfigureParameter("x").Consuming(Domain.Exhaustive);
        method
            .Throws<AssertionException>(when: args => args.At<uint>(0) % 2 == 1)
            .Expect(args => args[0], when: args => true);

        suite = builder.CreateSuite("chain");
        method = suite.RegisterMethod("CreateEnvelopeForTest");
        method.CofigureParameter(0).Consuming(Domain.Sequential, 1, 100);
        method.ConfigureParameter(1).Consuming(Domain.Custom, domain: () => new[]{"simple", "not simple"});
        method.ConfigureParameter(2).Consuming(Domain.Custom, yaff.Utility.RandomStringProvider.Words(from: 3, to: 5, count: 250));
        method.ConfigureParameter(3).Consuming(Domain.Custom, yaff.Utility.RandomStringProvider.Alphanumeric(3, 6, 100));

        // Process emulation - creates chain
        suite
            .RegisterMethod("IsProcessed")
            .Expect(args => args.At<Dictionary<string, string>>(0)["id"].Contains("1"))
            .ConfigureParameter(0)
            .Consuming(Domain.Output, "CreateEnvelopeForTest");
        suite
            .RegisterMethod("IsSimple")
            .DependsOn(name: "IsProcessed", result: false)
            .ConfigureParameter(0)
            .Consuming(Domain.Output, "CreateEnvelopeForTest");
        suite
            .RegisterMethod("ProcessSimple")
            .DependsOn("IsSimple", true)
            .ConfigureParameter(0)
            .Consuming(Domain.Output, "CreateEnvelopeForTest");
        suite
            .RegisterMethod("ProcessSpecial")
            .DependsOn("IsSimple", false)
            .ConfigureParameter(0)
            .Consuming(Domain.Output, "CreateEnvelopeForTest");

        // bonus
        suite
            .RegisterMethod("Process")
            .Expect(false, (target, args) => (target as ClassUnderTest)?.IsProcessed(args.At<Dictionary<string, string>>(0)))
            .Expect((target, args, runner) => runner.GetResult("ProcessSimple", target, args), (target, args, runner) => runner.GetResult("IsSimple", target, args).Equals(true))
            .ConfigureParameter(0)
            .Consuming(Domain.Output, "CreateEnvelopeForTest");

        return builder.CreateConfig();
    }

    public override object ExecuteCase(MethodInfo method, object target, object[] args){
        // enhancing basic executor - add timeout for case
        return Task.Run(() => base.ExecuteCase(method, target, args), TimeSpan.FromSeconds(3)).Result;
    }
}
```