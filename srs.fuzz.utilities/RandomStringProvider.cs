﻿using System.Collections.Generic;
using System.Linq;
using srs.fuzz.Internal;

namespace srs.fuzz.utilities
{
    /// <summary>
    /// utility class for random string generation
    /// </summary>
    public static class RandomStringProvider
    {
        private static readonly string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        /// <summary>
        /// create string using alphabetic chars (both lower and upper case)
        /// </summary>
        /// <param name="from">min length</param>
        /// <param name="to">max length</param>
        /// <param name="count">how many strings to generate</param>
        /// <returns></returns>
        public static IEnumerable<object> Alpha(int @from, int to, int count)
        {
            var set = Alphabet
                .ToCharArray()
                .Concat(Alphabet.ToLower().ToCharArray())
                .ToArray();
            return GenerateStrings(from, to, count, set);
        }
        /// <summary>
        /// generate string using alphabetic (lower+upper case) and numeric charactes
        /// </summary>
        /// <param name="from">min length</param>
        /// <param name="to">max length</param>
        /// <param name="count">how many strings to generate</param>
        /// <returns></returns>
        public static IEnumerable<object> Alphanumeric(int from, int to, int count)
        {
            var set = Alphabet
                .ToCharArray()
                .Concat(Alphabet.ToLower().ToCharArray())
                .Concat("1234567890".ToCharArray())
                .ToArray();
            return GenerateStrings(from, to, count, set);
        }
        /// <summary>
        /// strings generator
        /// </summary>
        /// <param name="from">min length</param>
        /// <param name="to">max length</param>
        /// <param name="count">how many to generate</param>
        /// <param name="set">char domain</param>
        /// <returns></returns>
        private static IEnumerable<object> GenerateStrings(int from, int to, int count, char[] set)
        {
            for (var i = 0; i < count; i++)
            {
                var len = Randomizer.GetInt(to + 1, from);
                var value = Enumerable.Range(0, len)
                        .Select(_ =>
                        {
                            var ix = Randomizer.GetInt(set.Length);
                            return set.At(ix);
                        })
                        .ToArray();
                yield return new string(value);
            }
        }
    }
}
