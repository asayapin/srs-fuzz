﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace srs.fuzz.hosts.nunit
{
    /// <summary>
    /// test runner host for NUnit
    /// </summary>
    public abstract class NUnitHost<T> where T : TestRunner, new()
    {
        private static readonly T Runner = new T();

        private static TestCaseData ToNUnitCase(ExceptionResult result)
        {
            return new TestCaseData(result?.Exception)
                .SetName(result?.Name ?? "all tests passed")
                .SetDescription(result?.Description ?? "")
                .SetCategory(result?.Category ?? "");
        }
        /// <summary>
        /// all cases from config
        /// </summary>
        public static IEnumerable<TestCaseData> ExceptionResults => Runner.GetExceptionResults().Select(ToNUnitCase);
        /// <summary>
        /// rethrow all exceptions from failed methods
        /// </summary>
        /// <param name="ex">exception to throw</param>
        [TestCaseSource(nameof(ExceptionResults))]
        public void FailedRuns(Exception ex)
        {
            if (ex is { }) throw ex;
        }
    }
}
