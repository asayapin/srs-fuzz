﻿namespace srs.fuzz
{
    /// <summary>
    /// service extensions
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// get array element at specified index
        /// </summary>
        /// <typeparam name="T">type to convert target item to</typeparam>
        /// <param name="o">target array</param>
        /// <param name="index">index to get at (0-based, wraps around array length, negative index corresponds to elements from end (that is, x.At(-1) gets last element))</param>
        /// <returns>item or default T value</returns>
        public static T At<T>(this object[] o, int index)
        {
            if (o.Length == 0) return default;
            var ix = index % o.Length;
            if (ix < 0) ix += o.Length;
            if (o[ix] is T t) return t;
            return default;
        }
        /// <summary>
        /// get array element at specified index
        /// </summary>
        /// <typeparam name="T">array type</typeparam>
        /// <param name="o">target array</param>
        /// <param name="index">index to get item at (0-based, wraps around array length, negative values converted to positive)</param>
        /// <returns></returns>
        public static T At<T>(this T[] o, int index)
        {
            if (o.Length == 0) return default;
            var ix = index % o.Length;
            if (ix < 0) ix += o.Length;
            if (o[ix] is T t) return t;
            return default;
        }
    }
}
