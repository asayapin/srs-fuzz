﻿namespace srs.fuzz
{
    /// <summary>
    /// recycle behavior
    /// </summary>
    public enum Recycle : byte
    {
        /// <summary>
        /// refresh instance on every item in domain
        /// </summary>
        PerCase, 
        /// <summary>
        /// refresh instance once per suite
        /// </summary>
        PerSuite, 
        /// <summary>
        /// create instance once for all suites
        /// </summary>
        PerRun
    }
    /// <summary>
    /// parameter domain type
    /// </summary>
    public enum Domain : byte
    {
        /// <summary>
        /// create domain with random items
        /// </summary>
        Random, 
        /// <summary>
        /// create domain with sequential items
        /// </summary>
        Sequential,
        /// <summary>
        /// use custom domain provider
        /// </summary>
        Custom,
        /// <summary>
        /// use output from other method as domain
        /// </summary>
        Output,
        /// <summary>
        /// create domain covering all possible values
        /// </summary>
        Exhaustive
    }
}
