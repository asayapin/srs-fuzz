﻿using System;

namespace srs.fuzz
{
    /// <summary>
    /// case run result enclosing thrown exception
    /// </summary>
    public class ExceptionResult
    {
        /// <summary>
        /// exception that was thrown by method
        /// </summary>
        public readonly Exception Exception;
        /// <summary>
        /// case name
        /// </summary>
        public readonly string Name;
        /// <summary>
        /// case verbose description
        /// </summary>
        public readonly string Description;
        /// <summary>
        /// case category (corresponds to suite name)
        /// </summary>
        public readonly string Category;

        internal ExceptionResult(Exception exception, string name, string description, string category)
        {
            Exception = exception;
            Name = name;
            Description = description;
            Category = category;
        }
    }
}