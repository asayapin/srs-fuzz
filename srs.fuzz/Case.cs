﻿using srs.fuzz.Internal;

namespace srs.fuzz
{
    /// <summary>
    /// test case container
    /// </summary>
    public class Case
    {
        /// <summary>
        /// target method
        /// </summary>
        public readonly Method Method;
        /// <summary>
        /// target instance
        /// </summary>
        public readonly object Target;
        /// <summary>
        /// args to pass to method
        /// </summary>
        public readonly object[] Args;
        /// <summary>
        /// suite name - for host use
        /// </summary>
        public readonly string SuiteName;
        /// <summary>
        /// test name - for host use
        /// </summary>
        public readonly string TestName;
        /// <summary>
        /// case details - for host use
        /// </summary>
        /// <remarks>
        /// contains detailed information on case - method name, target, args, expectations
        /// </remarks>
        public readonly string Details;
        /// <summary>
        /// construct test case
        /// </summary>
        /// <param name="method">method to invoke</param>
        /// <param name="target">target object</param>
        /// <param name="args">args to pass to method</param>
        /// <param name="suiteName">current suite name</param>
        /// <param name="testName">current test name</param>
        /// <param name="details">detailed case description</param>
        internal Case(Method method, object target, object[] args, string suiteName, string testName, string details = null)
        {
            Method = method;
            Target = target;
            Args = args;
            SuiteName = suiteName;
            TestName = testName;
            Details = details;
        }
    }
}