﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using srs.fuzz.Internal;

namespace srs.fuzz
{
    /// <summary>
    /// core class for tests running
    /// </summary>
    public abstract class TestRunner
    {
        private readonly ConcurrentDictionary<ResultsCacheKey, RunResult> _results = new ConcurrentDictionary<ResultsCacheKey, RunResult>();
        private int Iterations = 0;
        /// <summary>
        /// configuration provider - to be overridden in subclasses
        /// </summary>
        /// <returns></returns>
        protected abstract TestConfig Configure();
        /// <summary>
        /// case executor
        /// </summary>
        /// <remarks>
        /// Core method to run case and collect result. Runs method, checks if expectations present and met, pushes result to result cache &amp; returns result
        /// 
        /// If no expectations provided, or all expectations are on exceptions, method is assumed to run correctly if it doesn't throw an exception
        /// </remarks>
        /// <exception cref="AssertionException">if no expectations were met</exception>
        /// <exception cref="Exception">if value doesn't match any positive expectations</exception>
        /// <param name="method">method to invoke</param>
        /// <param name="target">target instance</param>
        /// <param name="args">args to pass to method</param>
        /// <returns>method invocation result</returns>
        public virtual object ExecuteCase(Method method, object target, object[] args)
        {
            var key = new ResultsCacheKey(method.MethodInfo.Name, target, args);
            var expectations = method.Expectations.Where(x => x.Predicate(target, args, this)).ToArray();
            var valued = expectations.Where(x => !x.ExpectException).Select(x => x.Expectation(target, args, this)).ToArray();
            var exceptional = expectations.Where(x => x.ExpectException).ToArray();
            var rethrow = false;
            try
            {
                var result = method.MethodInfo.Invoke(target, args);
                _results[key] = new RunResult(result);
                var matching = valued.Where(x => Equals(x, result));
                if (matching.Any() || valued.Length == 0) return result;
                rethrow = true;
                var expectedValues = string.Join(", ", valued.Select(x => $"<{x}>"));
                throw new Exception(
                    $"result <{result}> doesn't match any of expectations:\n{expectedValues}");
            }
            catch (Exception e)
            {
                if (e is TargetInvocationException tie)
                    e = e.InnerException;
                _results[key] = new RunResult(e, true);
                if (rethrow) throw;
                var matching = exceptional.Where(x => x.ExpectedException == e.GetType());
                if (!matching.Any()) throw new AssertionException("unexpected exception", e);
                return e;
            }
        }
        /// <summary>
        /// fetch result from results cache
        /// </summary>
        /// <param name="name">method name</param>
        /// <param name="target">method target</param>
        /// <param name="args">method args</param>
        /// <param name="result">fetched result</param>
        /// <returns>true if result exists and is not an exception; false otherwise</returns>
        public bool TryGetResult(string name, object target, object[] args, out object result)
        {
            result = null;
            if (_results.TryGetValue(new ResultsCacheKey(name, target, args), out var res))
            {
                result = res.Result;
                return res.RunCompleted;
            }
            return false;
        }
        /// <summary>
        /// get all results for specified method
        /// </summary>
        /// <param name="name">method name</param>
        /// <returns>cached results</returns>
        internal IEnumerable<object> GetResults(string name) => _results.Where(x => x.Key.Method == name && x.Value.RunCompleted).Select(x => x.Value.Result);

        #region test-related
        /// <summary>
        /// create cases from config
        /// </summary>
        /// <param name="config">config to process</param>
        /// <returns>created cases</returns>
        private IEnumerable<Case> CreateCases(TestConfig config)
        {
            foreach (var suite in config.Suites)
                foreach (var chain in suite.Chains)
                {
                    _results.Clear();
                    foreach (var step in chain.Steps)
                        foreach (var method in step.Methods)
                        {
                            var parameters = method.MethodInfo.GetParameters();
                            if (parameters.Length == 0) continue;
                            var domains = method.Domains.Zip(parameters, (dp, p) => dp(this, p.ParameterType)).ToList();
                            var argSets = domains.First().Select(x => new[] { x });
                            argSets = domains.Skip(1).Aggregate(argSets,
                                (agg, dom) => agg.SelectMany(x => dom, (x, d) => x.Append(d).ToArray()));
                            foreach (var args in argSets)
                            {
                                var target = suite.Factory();
                                if (method.Dependencies.Length == 0
                                    || method.Dependencies.All(dep => TryGetResult(dep.Method, target, args, out var res) && Equals(res, dep.Expected)))
                                {
                                    yield return new Case(
                                        method,
                                        target,
                                        args,
                                        $"{method.MethodInfo.Name} #{Iterations}",
                                        BuildDescription(method, target, args),
                                        suite.Name);
                                    Interlocked.Increment(ref Iterations);
                                }
                            }
                        }
                }
        }
        /// <summary>
        /// build verbose case description
        /// </summary>
        /// <param name="method">method to be invoked</param>
        /// <param name="target">method target</param>
        /// <param name="args">method args</param>
        /// <returns>verbose description</returns>
        private string BuildDescription(Method method, object target, object[] args)
        {
            return $@"{method.MethodInfo.Name} on target {target} (#{target.GetHashCode()})
Args:
    {string.Join("\n\t", args)}
{method.Expectations.Count} expected results, including {method.Expectations.Where(x => x.ExpectException).Count()} expecting exception";
        }

        #endregion

        #region for host consumption
        /// <summary>
        /// exception-throwing cases
        /// </summary>
        private readonly List<ExceptionResult> ExceptionResults = new List<ExceptionResult>();
        /// <summary>
        /// run all cases and record erroneous ones
        /// </summary>
        public List<ExceptionResult> GetExceptionResults()
        {
#if DEBUG
            foreach (var @case in CreateCases(Configure())) RunCase(@case);
#else
            Parallel.ForEach(CreateCases(Configure()), RunCase); 
#endif
            if (ExceptionResults.Count == 0)
                ExceptionResults.Add(new ExceptionResult(null, $"all tests (#{Iterations}) passed", "", ""));
            return ExceptionResults;
        }

        private void RunCase(Case @case)
        {
            try
            {
                ExecuteCase(@case.Method, @case.Target, @case.Args);
            }
            catch (Exception ex)
            {
                if (ExceptionResults.Count == 1 && ExceptionResults[0] is null) ExceptionResults.Clear();
                ExceptionResults.Add(new ExceptionResult(ex, @case.TestName, @case.Details, @case.SuiteName));
            }
        }
        #endregion
    }
}
