﻿using System;
using System.Collections.Generic;

namespace srs.fuzz.Internal
{
    internal delegate object Expectation(object target, object[] args, TestRunner runner);

    internal delegate bool ExpectationPredicate(object target, object[] args, TestRunner runner);

    internal delegate IEnumerable<object> DomainProvider(TestRunner runner, Type parameterType);
}
