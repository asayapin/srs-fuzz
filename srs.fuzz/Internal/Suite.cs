﻿using System;

namespace srs.fuzz.Internal
{
    internal class Suite
    {
        public string Name;
        public Chain[] Chains;
        public Func<object> Factory;
    }
}
