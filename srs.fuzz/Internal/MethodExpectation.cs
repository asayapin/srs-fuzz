﻿using System;

namespace srs.fuzz.Internal
{
    internal class MethodExpectation
    {
        internal readonly Expectation Expectation;
        internal readonly ExpectationPredicate Predicate;
        internal bool ExpectException;
        internal Type ExpectedException;

        private MethodExpectation(Expectation expectation, ExpectationPredicate predicate)
        {
            Expectation = expectation;
            Predicate = predicate;
            ExpectException = false;
            ExpectedException = null;
        }

        private MethodExpectation(Type exception, ExpectationPredicate predicate)
        {
            ExpectException = true;
            Expectation = null;
            Predicate = predicate;
            ExpectedException = exception;
        }

        internal static MethodExpectation ForValue(Expectation expectation, ExpectationPredicate predicate) => new MethodExpectation(expectation, predicate);
        internal static MethodExpectation ForException<T>(ExpectationPredicate predicate) where T : Exception => new MethodExpectation(typeof(T), predicate);
    }
}
