﻿using System;
using System.Linq;

namespace srs.fuzz.Internal
{
    internal class ResultsCacheKey : IEquatable<ResultsCacheKey>
    {
        public readonly string Method;
        public readonly string Type;
        public readonly object Target;
        public readonly object[] Args;

        public ResultsCacheKey(string method, object target, object[] args)
        {
            Method = method;
            Type = target.GetType().ToString();
            Target = target;
            Args = args.ToArray();
        }

        public bool Equals(ResultsCacheKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Method == other.Method
                   && Type == other.Type
                   && Equals(Target, other.Target)
                   && (Args ?? new object[0]).SequenceEqual(other.Args ?? new object[0]);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((ResultsCacheKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Method != null ? Method.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Type != null ? Type.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Target != null ? Target.GetHashCode() : 0);
                if (Args is { })
                    hashCode = Args.Aggregate(hashCode, (hash, item) => unchecked((hash * 397) ^ (item?.GetHashCode() ?? 0)));
                return hashCode;
            }
        }
    }
}
