﻿using System;

namespace srs.fuzz.Internal
{
    internal static class Extensions
    {
        public static object ConvertObject(this Type t, object o)
        {
            // TODO : handle cases as downcast
            return t.IsInstanceOfType(o) ? o : Convert.ChangeType(o, t);
        }

        public static object Clamp(this object o, long lower, long upper)
        {
            var len = upper - lower;
            return o.GetType().ConvertObject(lower + Convert.ToInt64(o) % len);
        }
    }
}
