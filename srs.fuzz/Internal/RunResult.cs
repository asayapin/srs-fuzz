﻿namespace srs.fuzz.Internal
{
    internal class RunResult
    {
        public readonly object Result;
        public readonly bool RunCompleted;

        public RunResult(object result, bool hasThrown = false)
        {
            Result = result;
            RunCompleted = !hasThrown;
        }
    }
}
