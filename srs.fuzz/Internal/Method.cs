﻿using System.Collections.Generic;
using System.Reflection;

namespace srs.fuzz.Internal
{
    /// <summary>
    /// method container
    /// </summary>
    public class Method
    {
        /// <summary>
        /// method info that will be invoked on case run
        /// </summary>
        internal MethodInfo MethodInfo;
        /// <summary>
        /// parameters domains
        /// </summary>
        internal DomainProvider[] Domains;
        /// <summary>
        /// result expectations
        /// </summary>
        internal List<MethodExpectation> Expectations;
        internal MethodDependency[] Dependencies;
    }
}
