﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace srs.fuzz.Internal
{
    /// <summary>
    /// random numbers generator
    /// </summary>
    public static class Randomizer
    {
        private static readonly ConcurrentQueue<byte> Bytes = new ConcurrentQueue<byte>();

        private static readonly Dictionary<Type, Func<byte[], object>> AllowedTypes = new Dictionary<Type, Func<byte[], object>>
        {
            [typeof(byte)] = bytes => bytes[0],
            [typeof(sbyte)] = bytes => (sbyte)bytes[0],
            [typeof(short)] = bytes => BitConverter.ToInt16(bytes, 0),
            [typeof(ushort)] = bytes => BitConverter.ToUInt16(bytes, 0),
            [typeof(int)] = bytes => BitConverter.ToInt32(bytes, 0),
            [typeof(uint)] = bytes => BitConverter.ToUInt32(bytes, 0),
            [typeof(long)] = bytes => BitConverter.ToInt64(bytes, 0),
            [typeof(ulong)] = bytes => BitConverter.ToUInt64(bytes, 0),
            [typeof(Guid)] = bytes => new Guid(bytes)
        };

        private static readonly Dictionary<Type, int> Sizes = new Dictionary<Type, int>
        {
            [typeof(byte)] = sizeof(byte),
            [typeof(sbyte)] = sizeof(sbyte),
            [typeof(short)] = sizeof(short),
            [typeof(ushort)] = sizeof(ushort),
            [typeof(int)] = sizeof(int),
            [typeof(uint)] = sizeof(uint),
            [typeof(long)] = sizeof(long),
            [typeof(ulong)] = sizeof(ulong),
            [typeof(Guid)] = 16,
        };
        /// <summary>
        /// generate integer
        /// </summary>
        /// <param name="max">upper bound (exclusive)</param>
        /// <param name="min">lower bound</param>
        /// <returns></returns>
        public static int GetInt(int max, int min = 0)
        {
            var len = max - min;
            var tmp = Get<int>() % len + min;
            while (tmp < min) tmp += len;
            return tmp;
        }
        /// <summary>
        /// get number of specified type
        /// </summary>
        /// <typeparam name="T">type to obtain - on of registered types (<see cref="Guid"/>, signed &amp; unsigned <see cref="byte"/>, <see cref="short"/>, <see cref="int"/>, <see cref="long"/>)</typeparam>
        /// <returns></returns>
        public static T Get<T>() where T : struct
        {
            return Get(typeof(T)) is T t ? t : throw new InvalidOperationException("state is corrupted");
        }
        /// <summary>
        /// non-generic version of <see cref="Get{T}"/>
        /// </summary>
        /// <param name="targetType">type to obtain; see <see cref="Get{T}"/></param>
        /// <returns></returns>
        public static object Get(Type targetType)
        {
            if (!AllowedTypes.TryGetValue(targetType, out var converter))
                throw new ArgumentException($"type {targetType} is not allowed for random generation");
            return converter(FetchBytes(Sizes[targetType]));
        }

        private static byte[] FetchBytes(int count)
        {
            var ret = new byte[count];
            for (int i = 0; i < count; i++)
            {
                var filled = false;
                do
                {
                    if (Bytes.TryDequeue(out var b))
                    {
                        ret[i] = b;
                        filled = true;
                    }
                    else AddBytesToQueue();
                } while (!filled);
            }
            return ret;
        }

        private static void AddBytesToQueue() => Guid.NewGuid().ToByteArray().ToList().ForEach(Bytes.Enqueue);

    }
}
