﻿namespace srs.fuzz.Internal
{
    internal class MethodDependency
    {
        internal string Method { get; }
        internal object Expected { get; }

        public MethodDependency(string method, object expected)
        {
            Method = method;
            Expected = expected;
        }
    }
}
