﻿using System;
using System.Collections.Generic;
using System.Linq;
using srs.fuzz.Internal;

namespace srs.fuzz.Configuration
{
    /// <summary>
    /// helper for method configuration
    /// </summary>
    public class MethodConfigBuilder
    {
        internal string Name { get; }
        private readonly List<ParameterConfigBuilder> _parameters = new List<ParameterConfigBuilder>();
        private readonly List<MethodExpectation> _expectations = new List<MethodExpectation>();
        private readonly List<MethodDependency> _dependencies = new List<MethodDependency>();

        internal MethodDependency[] Dependencies => _dependencies.ToArray();
        internal ParameterConfigBuilder[] Parameters => _parameters.ToArray();
        internal List<MethodExpectation> Expectations => _expectations.ToList();

        internal MethodConfigBuilder(string name)
        {
            Name = name;
        }
        /// <summary>
        /// create config builder for parameter
        /// </summary>
        /// <param name="name">parameter name</param>
        /// <returns></returns>
        public ParameterConfigBuilder ConfigureParameter(string name)
        {
            var pcb = new ParameterConfigBuilder(name);
            _parameters.Add(pcb);
            return pcb;
        }
        /// <summary>
        /// create config builder for parameter
        /// </summary>
        /// <param name="index">parameter index in target method (0-based)</param>
        /// <returns></returns>
        public ParameterConfigBuilder ConfigureParameter(int index)
        {
            var pcb = new ParameterConfigBuilder(index.ToString());
            _parameters.Add(pcb);
            return pcb;
        }
        /// <summary>
        /// fluently add expectation for method invokation
        /// </summary>
        /// <param name="what">result factory; see <see cref="Expectation"/></param>
        /// <param name="when">expectation predicate; see <see cref="ExpectationPredicate"/></param>
        /// <returns></returns>
        public MethodConfigBuilder Expect(Func<object[], object> what, Func<object[], bool> when = null)
        {
            _expectations.Add(MethodExpectation.ForValue(
                (_, args, __) => what(args),
                (_, args, __) => when?.Invoke(args) != false));
            return this;
        }
        /// <summary>
        /// fluently add expectation of exception
        /// </summary>
        /// <typeparam name="T">exception to expect</typeparam>
        /// <param name="when">expectation predicate; <see cref="ExpectationPredicate"/></param>
        /// <returns></returns>
        public MethodConfigBuilder Throws<T>(Func<object[], bool> @when) where T : Exception
        {
            _expectations.Add(MethodExpectation.ForException<T>((_, args, __) => when(args)));
            return this;
        }
        /// <summary>
        /// specify method that should be ran prior to this method
        /// </summary>
        /// <param name="method">parent method</param>
        /// <param name="expected">expected result</param>
        /// <returns></returns>
        public MethodConfigBuilder DependsOn(string method, object expected)
        {
            _dependencies.Add(new MethodDependency(method, expected));
            return this;
        }
        /// <summary>
        /// fluently add expectation for method invokation
        /// </summary>
        /// <param name="what">result to expect; see <see cref="Expectation"/></param>
        /// <param name="when">expectation predicate; see <see cref="ExpectationPredicate"/></param>
        /// <returns></returns>
        public MethodConfigBuilder Expect(object what, Func<object, object[], bool> @when)
        {
            _expectations.Add(MethodExpectation.ForValue(
                (_, __, ___) => what,
                (target, args, _) => when(target, args)
                ));
            return this;
        }
        /// <summary>
        /// fluently add expectation for method invokation
        /// </summary>
        /// <param name="what">result factory; see <see cref="Expectation"/></param>
        /// <param name="when">expectation predicate; see <see cref="ExpectationPredicate"/></param>
        /// <returns></returns>
        public MethodConfigBuilder Expect(Func<object, object[], TestRunner, object> what,
            Func<object, object[], TestRunner, bool> when)
        {
            _expectations.Add(MethodExpectation.ForValue(
                new Expectation(what), 
                new ExpectationPredicate(when)));
            return this;
        }
    }
}