﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace srs.fuzz.Configuration
{
    /// <summary>
    /// config build helper
    /// </summary>
    public class ConfigBuilder
    {
        private readonly Func<object> _factory;
        private readonly Recycle _recycle;
        private readonly List<TestSuite> _suites = new List<TestSuite>();

        private ConfigBuilder(Func<object> factory, Recycle recycle)
        {
            _factory = factory;
            _recycle = recycle;
        }
        /// <summary>
        /// convert builder to config consumable by test runner
        /// </summary>
        /// <returns>config declared using builder</returns>
        public TestConfig CreateConfig()
        {
            Func<object> instanceProvider;
            switch (_recycle)
            {
                case Recycle.PerCase:
                    instanceProvider = _factory;
                    break;
                case Recycle.PerRun:
                    var instance = _factory();
                    instanceProvider = () => instance;
                    break;
                default:
                    instanceProvider = null;
                    break;
            }
            return new TestConfig {Suites = _suites.Select(x => x.ToSuite(instanceProvider, _factory)).ToArray()};
        }
        /// <summary>
        /// specify target object factory
        /// </summary>
        /// <typeparam name="T">target object type</typeparam>
        /// <param name="factory"></param>
        /// <param name="recycle"></param>
        /// <returns></returns>
        public static ConfigBuilder For<T>(Func<T> factory, Recycle recycle) => new ConfigBuilder(() => factory(), recycle);
        /// <summary>
        /// add and get new suite for config
        /// </summary>
        /// <param name="name">suite name (outputs as category in test runner)</param>
        /// <returns>suite configurator</returns>
        public TestSuite CreateSuite(string name)
        {
            var suite = new TestSuite(name);
            _suites.Add(suite);
            return suite;
        }
    }
}
