﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using srs.fuzz.Internal;

namespace srs.fuzz.Configuration
{
    /// <summary>
    /// test suite config builder
    /// </summary>
    public class TestSuite
    {
        private const BindingFlags Flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                   BindingFlags.Static;

        private readonly string _name;
        private Func<object> _factory;
        private Recycle _recycle;
        private readonly List<MethodConfigBuilder> _methods = new List<MethodConfigBuilder>();

        internal TestSuite(string name)
        {
            _name = name;
        }
        /// <summary>
        /// specify suite-scoped instance factory
        /// </summary>
        /// <typeparam name="T">instance type</typeparam>
        /// <param name="factory">instance factory</param>
        /// <param name="recycle">recycle behavior</param>
        /// <returns></returns>
        public TestSuite With<T>(Func<T> factory, Recycle recycle)
        {
            _factory = () => factory();
            _recycle = recycle;
            return this;
        }
        /// <summary>
        /// register new method for suite
        /// </summary>
        /// <param name="name">method name (use nameof() to avoid errors)</param>
        /// <returns></returns>
        public MethodConfigBuilder RegisterMethod(string name)
        {
            var mcb = new MethodConfigBuilder(name);
            _methods.Add(mcb);
            return mcb;
        }

        internal Suite ToSuite(Func<object> instanceProvider, Func<object> factory)
        {
            // NOTE : treat null as PerSuite recycle scheme
            if (instanceProvider is null)
            {
                var instance = factory();
                instanceProvider = () => instance;
            }

            if (_factory is { })
            {
                switch (_recycle)
                {
                    case Recycle.PerCase:
                        instanceProvider = _factory;
                        break;
                    case Recycle.PerSuite:
                        var instance = _factory();
                        instanceProvider = () => instance;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            var type = instanceProvider().GetType();

            return new Suite
            {
                Factory = instanceProvider,
                Name = _name,
                Chains = BuildChains(type)
            };
        }

        private Chain[] BuildChains(Type type)
        {
            // build graphs encapsulating each chain; since every chain is representable as a tree, step is tree level
            // two relations are present: dependency and feed
            // following dict is [method -> consumers]; every item in list *depends* on item in key
            var dependents = new Dictionary<string, List<string>>();
            foreach (var method in _methods)
            {
                dependents[method.Name] = new List<string>();
                var dependencies = method.Dependencies
                    .Select(x => x.Method)
                    .Concat(method.Parameters
                        .Where(x => x.Domain == Domain.Output)
                        .Select(x => x.DependsOn));
                foreach (var dependencyName in dependencies)
                {
                    if (!dependents.ContainsKey(dependencyName))
                        dependents[dependencyName] = new List<string>();
                    dependents[dependencyName].Add(method.Name);
                }
            }
            if (dependents.Count == 0) return new Chain[0];
            return CreateChainGraphs(dependents)
                .Select(CreateTiers)
                .Select(
                    x => new Chain
                    {
                        Steps = x.Select(
                            ls => new Step
                            {
                                Methods = ls
                                    .Select(name => _methods.First(mcb => mcb.Name == name))
                                    .Select(mcb => ToMethod(mcb, type))
                                    .ToArray()
                            }).ToArray()
                    })
                .ToArray();
        }

        private static List<Dictionary<string, List<string>>> CreateChainGraphs(Dictionary<string, List<string>> dependents)
        {
            var topLevelNodes = dependents.Where(x => IsTopLevel(dependents, x.Key));
            var graphs = new List<Dictionary<string, List<string>>>();
            graphs.AddRange(
                topLevelNodes
                    .Select(x => new Dictionary<string, List<string>>
                    {
                        [x.Key] = x.Value
                    }));
            foreach (var key in graphs.Select(x => x.First().Key)) dependents.Remove(key);
            while (dependents.Count > 0)
                foreach (var (key, dependentsList) in dependents.ToList())
                {
                    var target = graphs.FirstOrDefault(x => x.Values.Any(v => v.Contains(key)));
                    if (target is null) continue;
                    target.Add(key, dependentsList);
                    dependents.Remove(key);
                }

            return graphs;
        }

        private static Method ToMethod(MethodConfigBuilder method, Type type)
        {
            var methods = type.GetMethods(Flags).Where(x => x.Name == method.Name).ToArray();

            if (methods.Length > 1)
                methods = methods.Where(x => Match(x, method.Parameters)).ToArray();
            var methodInfo = methods.Single();
            var parameters = OrderParameters(methodInfo, method.Parameters);

            var m = new Method{
                Domains = parameters.Select(x => x.Provider).ToArray(),
                MethodInfo = methodInfo,
                Expectations = method.Expectations,
                Dependencies = method.Dependencies
            };
            return m;
        }

        private static bool Match(MethodInfo method, ParameterConfigBuilder[] parameters)
        {
            var param = method.GetParameters();
            if (param.Length != parameters.Length) return false;
            return parameters
                .Where(x => !int.TryParse(x.NameOrIndex, out _))
                .All(p => param.Any(x => x.Name == p.NameOrIndex));
        }

        private static ParameterConfigBuilder[] OrderParameters(MethodInfo method, ParameterConfigBuilder[] parameters)
        {
            var methodParams = method.GetParameters().ToList();
            var param = new ParameterConfigBuilder[methodParams.Count];
            foreach (var p in parameters)
            {
                if (int.TryParse(p.NameOrIndex, out var index))
                    param[index] = p;
                else
                {
                    var i = methodParams.FindIndex(x => x.Name == p.NameOrIndex);
                    if (i > -1)
                        param[i] = p;
                }
            }
            if (param.Where((x, i) => x is null && IsRequired(methodParams[i])).Any())
                throw new ArgumentException("parameters passed don't fill all required parameters");
            return param;
        }

        private static bool IsRequired(ParameterInfo p)
        {
            return p.IsIn && !p.HasDefaultValue && !p.IsOptional;
        }

        private static List<List<string>> CreateTiers(Dictionary<string, List<string>> graph)
        {
            var dict = new Dictionary<string, List<string>>(graph);
            var tiers = new List<List<string>>();
            while (dict.Count > 0)
            {
                var topLevel = dict.Where(x => IsTopLevel(dict, x.Key)).Select(x => x.Key).ToList();
                tiers.Add(topLevel);
                topLevel.ForEach(x => dict.Remove(x));
            }
            return tiers;
        }

        private static bool IsTopLevel(Dictionary<string, List<string>> dict, string key)
        {
            return !dict.Where(x => x.Key != key).Any(x => x.Value.Contains(key));
        }
    }
}