﻿using System;
using System.Collections.Generic;
using System.Linq;
using srs.fuzz.Internal;

namespace srs.fuzz.Configuration
{
    /// <summary>
    /// config builder helper for method parameters
    /// </summary>
    public class ParameterConfigBuilder
    {
        internal string NameOrIndex { get; }
        internal Domain Domain { get; private set; }
        internal DomainProvider Provider { get; private set; }
        internal string DependsOn { get; private set; }

        internal ParameterConfigBuilder(string nameOrIndex)
        {
            NameOrIndex = nameOrIndex;
        }
        /// <summary>
        /// configure parameter domain as numeric sequence
        /// </summary>
        /// <param name="from">lower sequence bound</param>
        /// <param name="to">upper sequence bound</param>
        /// <returns></returns>
        public ParameterConfigBuilder ConsumingSequential(long @from, long to)
        {
            Domain = Domain.Sequential;
            Provider = (_, type) => DomainProvider(type, from, to - 1);
            return this;
        }
        private static IEnumerable<object> DomainProvider(Type type, long from, long to)
        {
            for (var i = @from; i <= to; i++) yield return type.ConvertObject(i);
        }
        /// <summary>
        /// configure parameter domain as random numeric sequence
        /// </summary>
        /// <param name="from">lower bound</param>
        /// <param name="to">upper bound (exclusive)</param>
        /// <param name="count">how many numbers to generate</param>
        /// <returns></returns>
        public ParameterConfigBuilder ConsumingRandom(long from, long to, int count)
        {
            Domain = Domain.Random;
            Provider = (_, type) => Enumerable.Range(0, count)
                .Select(x => Randomizer.Get(type).Clamp(from, to))
                .Select(type.ConvertObject);
            return this;
        }
        /// <summary>
        /// configure parameter domain as all possible values of parameter's type
        /// </summary>
        /// <returns></returns>
        public ParameterConfigBuilder ConsumingExhaustive()
        {
            Domain = Domain.Exhaustive;
            Provider = (_, type) => CreateDomain(type);
            return this;

            IEnumerable<object> CreateDomain(Type type)
            {
                try
                {
                    var min = (long)Convert.ChangeType(type.GetField("MinValue").GetValue(null), typeof(long));
                    var max = (long)Convert.ChangeType(type.GetField("MaxValue").GetValue(null), typeof(long));
                    return DomainProvider(type, min, max);
                }
                catch
                {
                    return new object[0];
                }
            }
        }
        /// <summary>
        /// configure parameter domain with custom domain provider
        /// </summary>
        /// <param name="domain">custom domain</param>
        /// <returns></returns>
        public ParameterConfigBuilder ConsumingCustom(IEnumerable<object> domain)
        {
            Domain = Domain.Custom;
            Provider = (_, type) => domain.Select(type.ConvertObject);
            return this;
        }
        /// <summary>
        /// configure parameter domain as method's output
        /// </summary>
        /// <param name="name">method to take output from</param>
        /// <returns></returns>
        public ParameterConfigBuilder ConsumingOutput(string name)
        {
            DependsOn = name;
            Domain = Domain.Output;
            Provider = (runner, type) => runner.GetResults(name).Select(type.ConvertObject);
            return this;
        }
    }
}