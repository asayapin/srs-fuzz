# SRS Fuzz

[![pipeline status](https://gitlab.com/asayapin/srs-fuzz/badges/master/pipeline.svg)](https://gitlab.com/asayapin/srs-fuzz/-/commits/master)
[![coverage report](https://gitlab.com/asayapin/srs-fuzz/badges/master/coverage.svg)](https://gitlab.com/asayapin/srs-fuzz/-/commits/master)
[![docs](https://img.shields.io/badge/docs-gitlab_pages-blue)](https://asayapin.gitlab.io/srs-fuzz)

package|version
---|---
core|[![srs.fuzz package in package-feed feed in Azure Artifacts](https://feeds.dev.azure.com/asajapin/_apis/public/Packaging/Feeds/3833ade8-fc2b-4851-9b9c-bc773d6c9b1d/Packages/956c2c5b-7311-4926-b1cb-a43198854482/Badge)](https://dev.azure.com/asajapin/nuget%20server/_packaging?_a=package&feed=3833ade8-fc2b-4851-9b9c-bc773d6c9b1d&package=956c2c5b-7311-4926-b1cb-a43198854482&preferRelease=true)
utilities|[![srs.fuzz.utilities package in package-feed feed in Azure Artifacts](https://feeds.dev.azure.com/asajapin/_apis/public/Packaging/Feeds/3833ade8-fc2b-4851-9b9c-bc773d6c9b1d/Packages/2a0a5108-491e-4557-93a2-633e462f5103/Badge)](https://dev.azure.com/asajapin/nuget%20server/_packaging?_a=package&feed=3833ade8-fc2b-4851-9b9c-bc773d6c9b1d&package=2a0a5108-491e-4557-93a2-633e462f5103&preferRelease=true)
hosts.nunit|[![srs.fuzz.hosts.nunit package in package-feed feed in Azure Artifacts](https://feeds.dev.azure.com/asajapin/_apis/public/Packaging/Feeds/3833ade8-fc2b-4851-9b9c-bc773d6c9b1d/Packages/50771705-b96c-4830-92f1-3e4fe6f3b58a/Badge)](https://dev.azure.com/asajapin/nuget%20server/_packaging?_a=package&feed=3833ade8-fc2b-4851-9b9c-bc773d6c9b1d&package=50771705-b96c-4830-92f1-3e4fe6f3b58a&preferRelease=true)

Simple fuzzing microframework

## How it works

Package provides *hosts*, which are target to be inherited. Each host is configured for specific test engine (nUnit, xUnit etc),
and itself is inherited from `TestRunner`.

When test runner is launched and host inheritor is discovered, method `RunTests` is invoked, which fetches test cases from internal source,
which traverses configuration provided with `Configure` method

Each case is ran, result is cached (for method chains); once chain is ran, cache is cleared

### Configuration

Each host provides abstract `Configure` method returning `TestConfig`, which can be created using `ConfigBuilder.CreateConfig` instance method

Each configuration consists of:
- instance factory with recycle behavior (single instance for all cases, instance per suite, instance per case)
  - used to create target for method invokation
- 1+ test suite
  - used to encapsulate group of somehow related tests

Each suite is a set of method chains; additionally, suite can declare instance factory and recycle behavior (per suite/per case only)

Method chains are used to declare dependencies between methods (DEPENDS ON and GETS VALUE FROM). Each chain may contain from 1+ method definition

Each method definition contains
- method name (what would be invoked on target)
- parameters definitions

Each parameter definition is a pair of parameter name/index and domain definition

Domain definition can be one of the following:
- numeric types only
  - `Exhaustive` - run method for all values from type domain (`0..255` for byte, `0..65535` for ushort)
  - `Sequential` - all values from specified range [from..to)
  - `Random` - `x` random values from range [from..to)
- any type
  - `Custom` - gets values from passed `IEnumerable`
  - `Output` - gets values from other method's output

#### Method dependencies

Chain are used to encapsulate groups of methods that should be run on same target and in specific order.

Two dependency relationship types are supported:
- DEPENDS ON - method should be run only if specified dependency method was run and returned specified result
  
  `methodA.DependsOn("methodB", true)` - methodA runs only for inputs, on which methodB returns `true`
- GETS VALUE FROM - method's parameter domain is a set of outputs of a specific method. This relationship is configured on parameter

  `methodA.Parameter("first").ConsumingOutput("methodB")` - when running methodA, parameter `first`'s domain will be created using distinct values from `methodB`'s output

## How to use

In few simple steps:

1. Inherit a class from appropriate host (currently only `nUnit`)
```
    public class Tests : NUnitHost {

    }
```
2. implement `Configure` (see samples)
3. *optional* override `ExecuteCase` (don't forget to call base implementation)
4. done! you can launch test runner, which should be able to pick test classes and run all cases declared in `Configure`

## TODO

- add `PerChain` behavior for target factory