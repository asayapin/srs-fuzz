# SRS Fuzz

[![pipeline status](https://gitlab.com/asayapin/srs-fuzz/badges/master/pipeline.svg)](https://gitlab.com/asayapin/srs-fuzz/-/commits/master)
[![coverage report](https://gitlab.com/asayapin/srs-fuzz/badges/master/coverage.svg)](https://gitlab.com/asayapin/srs-fuzz/-/commits/master)

package|version
---|---
core|[![srs.fuzz package in package-feed feed in Azure Artifacts](https://feeds.dev.azure.com/asajapin/_apis/public/Packaging/Feeds/3833ade8-fc2b-4851-9b9c-bc773d6c9b1d/Packages/956c2c5b-7311-4926-b1cb-a43198854482/Badge)](https://dev.azure.com/asajapin/nuget%20server/_packaging?_a=package&feed=3833ade8-fc2b-4851-9b9c-bc773d6c9b1d&package=956c2c5b-7311-4926-b1cb-a43198854482&preferRelease=true)
utilities|[![srs.fuzz.utilities package in package-feed feed in Azure Artifacts](https://feeds.dev.azure.com/asajapin/_apis/public/Packaging/Feeds/3833ade8-fc2b-4851-9b9c-bc773d6c9b1d/Packages/2a0a5108-491e-4557-93a2-633e462f5103/Badge)](https://dev.azure.com/asajapin/nuget%20server/_packaging?_a=package&feed=3833ade8-fc2b-4851-9b9c-bc773d6c9b1d&package=2a0a5108-491e-4557-93a2-633e462f5103&preferRelease=true)
hosts.nunit|[![srs.fuzz.hosts.nunit package in package-feed feed in Azure Artifacts](https://feeds.dev.azure.com/asajapin/_apis/public/Packaging/Feeds/3833ade8-fc2b-4851-9b9c-bc773d6c9b1d/Packages/50771705-b96c-4830-92f1-3e4fe6f3b58a/Badge)](https://dev.azure.com/asajapin/nuget%20server/_packaging?_a=package&feed=3833ade8-fc2b-4851-9b9c-bc773d6c9b1d&package=50771705-b96c-4830-92f1-3e4fe6f3b58a&preferRelease=true)

Simple micro-framework for fuzzing tests

## Purpose

> Testing shows the presence, not the absence of bugs
> 
> *E.W. Dijkstra*

The framework was created primarily to allow fuzzing tests of APIs, to extend capabilities of unit tests.

Main point is it's impossible to test all cases, nor it's possible to find all cases that will lead to bugs and
document them as tests - there always will be something beyond expected user activities and API usage.

The framework is intended to allow somewhat exhaustive testing of provided methods