# SRS Fuzz

[![pipeline status](https://gitlab.com/asayapin/srs-fuzz/badges/master/pipeline.svg)](https://gitlab.com/asayapin/srs-fuzz/-/commits/master)
[![coverage report](https://gitlab.com/asayapin/srs-fuzz/badges/master/coverage.svg)](https://gitlab.com/asayapin/srs-fuzz/-/commits/master)

Simple micro-framework for fuzzing tests

## Purpose

> Testing shows the presence, not the absence of bugs
> E.W. Dijkstra

The framework was created primarily to allow fuzzing tests of APIs, to extend capabilities of unit tests.

Main point is it's impossible to test all cases, nor it's possible to find all cases that will lead to bugs and
document them as tests - there always will be something beyond expected user activities and API usage.

The framework is intended to allow somewhat exhaustive testing of provided methods